Git будет игнорировать локальные файлы Terraform, .tfstate файлы, crash логи Terraform и прочие файлы связанные с Terraform, на любом уровне вложенности, которые нежелательно загружать в репозиторий.
